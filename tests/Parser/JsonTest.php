<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 20/10/2016
 * Time: 13:39
 */
namespace Pixasia\Parser;

/**
 * Class Config to handle Config files
 *
 * Pixasia\Config class to handle loading files and manipulating configs files
 * Loads config files from JSON in v1.0
 *
 * @package     Pixasia\Config
 * @author      Sam Tidball <sam@pixedo.co.uk>
 * @version     1.0
 * @since       1.0
 *
 */
class JsonTest extends \PHPUnit\Framework\TestCase
{

    const   INVALID_JSON    = 'tests/Mocks/Json/missing_file.json';
    const   INVALID_FILE    = 'tests/Mocks/Json/invalid.json';
    const   MISMATCHED_JSON = 'tests/Mocks/Json/mismatched.json';
    const   VALID_FILE      = 'tests/Mocks/Json/valid.json';

    /**
     * @covers \Pixasia\Parser\Json::parse
     * @covers \Pixasia\Parser\Json::load
     * @covers \Pixasia\Parser\Json::parseContent
     */
    public function testParse()
    {
        $contents = Json::parse(self::VALID_FILE);

        $this->assertInternalType('object', $contents);
    }

    /**
     * @covers \Pixasia\Parser\Json::parse()
     * @covers \Pixasia\Parser\Json::load()
     *
     * @expectedException \Exception
     */
    public function testFailParse()
    {
        Json::parse(self::INVALID_FILE);
    }

    /**
     * @covers \Pixasia\Parser\Json::parse()
     * @covers \Pixasia\Parser\Json::load()
     *
     * @expectedException \Exception
     */
    public function testParseNoFile()
    {
        Json::parse(NULL);
    }

    /**
     * @covers \Pixasia\Parser\Json::parse()
     * @covers \Pixasia\Parser\Json::load()
     *
     * @expectedException \Exception
     */
    public function testParseInvalidJson()
    {
        Json::parse(self::INVALID_JSON);
    }

    /**
     * @covers \Pixasia\Parser\Json::parseContent
     *
     * @expectedException \Exception
     */
    public function testParseMismatchedJson(){
        Json::parse(self::MISMATCHED_JSON);
    }

}