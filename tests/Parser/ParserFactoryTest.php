<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 31/10/2016
 * Time: 13:34
 */

namespace Pixasia\Parser;

/**
 * Class Config to handle Config files
 *
 * Pixasia\Config class to handle loading files and manipulating configs files
 * Loads config files from JSON in v1.0
 *
 * @package     Pixasia\Config
 * @author      Sam Tidball <sam@pixedo.co.uk>
 * @version     1.0
 * @since       1.0
 *
 */
class ParserFactoryTest extends \PHPUnit\Framework\TestCase{

    /**
     * @covers \Pixasia\Parser\ParserFactory::getInstance
     * @covers \Pixasia\Parser\ParserFactory::getExtension
     * @covers \Pixasia\Parser\ParserFactory::getParserClassName
     *
     * @throws \Exception
     */
    public function testGetInstance(){
        $parser = ParserFactory::getInstance('something.json');

        $this->assertInstanceOf('\\Pixasia\\Parser\\Json', $parser);
    }

    /**
     * @covers \Pixasia\Parser\ParserFactory::getInstance
     * @covers \Pixasia\Parser\ParserFactory::getExtension
     * @covers \Pixasia\Parser\ParserFactory::getParserClassName
     *
     * @expectedException \Exception
     */
    public function testInvalidInstance(){
        ParserFactory::getInstance('valid.file');
    }

}