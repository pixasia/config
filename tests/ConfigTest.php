<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 19/10/2016
 * Time: 19:11
 */

namespace Pixasia;
use Pixasia\Parser\IParser;

/**
 * Class Config to handle Config files
 *
 * Pixasia\Config class to handle loading files and manipulating configs files
 * Loads config files from JSON in v1.0
 *
 * @package     Pixasia\Config
 * @author      Sam Tidball <sam@pixedo.co.uk>
 * @version     1.0
 * @since       1.0
 *
 */
class ConfigTest extends \PHPUnit\Framework\TestCase
{
    const   INVALID_FILE    = 'tests/Mocks/Json/invalid.json';
    const   VALID_FILE      = 'tests/Mocks/Json/valid.json';
    const   MISMATCHED_JSON = 'tests/Mocks/Json/mismatched.json';
    const   NO_ENVIRONMENT  = 'tests/Mocks/Json/no_environment.json';
    const   INVALID_TYPE    = 'unsupport.file';

    const   VALID_JSON = '{"development": {"foo": "bar","multi": {"a": "first","b": "second"}}}';

    /**
     * @return \Pixasia\Parser\Json
     */
    private function getParserMock(){
        $mock = $this->getMockBuilder('\\Pixasia\\Parser\\Json')
          ->setMethods(['parse'])
          ->getMock();


        $mock
          ->method('parse')
          ->willReturn((array)json_decode(self::VALID_JSON));

        return $mock;
    }

    /**
     * Return a mock parser
     * @return IParser
     */
    private function getParserFactoryMock(){
        $mock = $this->getMockBuilder('\\Pixasia\\Parser\\ParserFactory')
          ->setMethods(['getInstance'])
          ->getMock();


        $mock
          ->method('getInstance')
          ->willReturn(new Parser\Json());

        return $mock;
    }

    /**
     * @covers \Pixasia\Config::__construct
     *
     * @expectedException \Exception
     */
    public function testFailLoad()
    {
        new Config(self::INVALID_FILE);
    }

    /**
     * @covers \Pixasia\Config::__construct
     *
     * Test that a valid file can be loaded
     */
    public function testLoad()
    {
        new Config(self::VALID_FILE);
    }

    /**
     * @covers \Pixasia\Config::__construct
     *
     * Test that a valid file can be loaded
     *
     * @expectedException \Exception
     */
    public function testInvalidConfigFile()
    {
        new Config(self::MISMATCHED_JSON);
    }


    /**
     * @covers \Pixasia\Config::loadConfig
     * @covers \Pixasia\Config::setConfig
     *
     * @expectedException \Exception
     */
    public function testInvalidSetConfig()
    {
        $mock = $this->getMockBuilder('\\Pixasia\\Parser\\Json')
          ->setMethods(['parse'])
          ->getMock();

        $mock
          ->method('parse')
          ->willReturn(NULL);

        Config::loadConfig($mock->parse(self::VALID_FILE));
    }

    /**
     * @covers \Pixasia\Config::__construct
     * @covers \Pixasia\Parser\ParserFactory::getInstance
     *
     * @expectedException \Exception
     */
    public function testInvalidFileType()
    {
        new Config(self::INVALID_TYPE);
    }

    /**
     * @covers \Pixasia\Config::loadConfig
     * @covers \Pixasia\Config::setConfig
     * @covers \Pixasia\Config::setEnvironment
     *
     * @expectedException \Exception
     */
    public function testInvalidEnvironment()
    {
        new Config(self::NO_ENVIRONMENT);
    }

    /**
     * @covers \Pixasia\Config::loadConfig
     * @covers \Pixasia\Config::setConfig
     */
    public function testsetConfigArrayToObject()
    {
        $mock = $this->getMockBuilder('\\Pixasia\\Parser\\Json')
          ->setMethods(['parse'])
          ->getMock();

        $mock
          ->method('parse')
          ->willReturn((array)json_decode(self::VALID_JSON));

        new Config(self::VALID_FILE);
    }


    /**
     * @covers \Pixasia\Config::__construct
     * @covers \Pixasia\Config::getContents
     * @covers \Pixasia\Config::loadConfig
     * @covers \Pixasia\Config::setConfig
     * @covers \Pixasia\Config::getEnvironment
     * @covers \Pixasia\Config::get
     *
     * @covers \Pixasia\Parser\ParserFactory::getInstance
     *
     * @throws \Exception
     */
    public function testGetEnvironment()
    {
        new Config(self::VALID_FILE);

        $this->assertEquals('development', Config::getEnvironment());
        $this->assertEquals('127.0.0.1', Config::get('host'));
        $this->assertEquals('s3cr3t', Config::get('secret'));
        $this->assertEquals('localhost', Config::get('database.host'));
        $this->assertEquals('fallback', Config::get('invalid','fallback'));
    }

    /**
     * @covers \Pixasia\Config::setEnvironment
     *
     * @expectedException \Exception
     */
    public function testSetInvalidEnvironment(){
        $mock = $this->getMockBuilder('\\Pixasia\\Parser\\Json')
          ->setMethods(['parse'])
          ->getMock();

        $mock
          ->method('parse')
          ->willReturn(NULL);

        Config::loadConfig($mock->parse(self::VALID_FILE));
    }

    /**
     * @covers \Pixasia\Config::__construct
     * @covers \Pixasia\Config::get
     *
     * @expectedException \Exception
     */
    public function testInvalidProperty()
    {
        new Config(self::VALID_FILE);
        Config::get('fake_value');
    }
}
