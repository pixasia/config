<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 17/08/2016
 * Time: 18:37
 */

namespace Pixasia;
use Pixasia\Parser\IParser;
use Pixasia\Parser\ParserFactory;

/**
 * Class Config to handle Config files
 *
 * Pixasia\Config class to handle loading files and manipulating configs files
 * Loads config files from JSON in v1.0
 *
 * @package     Pixasia\Config
 * @author      Sam Tidball <sam@pixedo.co.uk>
 * @version     1.0
 * @since       1.0
 *
 */
class Config
{

    /** @var null $config */
    public static $config = null;

    /** @var string $environment The environment */
    private static $environment = null;


    /**
     * Constants for environments
     */
    const DEVELOPMENT = 'development';
    const STAGING = 'staging';
    const LIVE = 'live';

    /**
     * Find the current environment by looking through the current config and
     * trying to match an environment variable to a configuration.
     *
     * @param string $env The environment variable to use. E.g. development
     *
     * @return bool
     */
    private static function findEnvironmentByEnv($env)
    {
        foreach (self::$config as $environment => $config) {
            if ($environment == $env) {
                self::$environment = $environment;
                return true;
            }
        }
        return false;
    }

    /**
     * Find the current environment by looking through the current config and
     * trying to match a hostname to a configuration.
     *
     * @param string $host The hostname to use. E.g. pixedo.co.uk
     *
     * @return bool
     */
    private static function findEnvironmentByURL($host)
    {
        //Loop through each key in the config
        foreach (self::$config as $environment => $config) {
            if ( isset($config->host) && $config->host == $host) {
                self::$environment = $environment;
                return true;
            }
        }
        return false;
    }

    /**
     * Set the environment
     *
     * @return bool
     * @throws \Exception
     */
    private static function setEnvironment()
    {
        if (isset($_ENV['APP_ENV']) && self::findEnvironmentByEnv($_ENV['APP_ENV'])) {
        } elseif (isset($_SERVER['SERVER_NAME']) && self::findEnvironmentByURL($_SERVER['SERVER_NAME'])) {

        } else {
            throw new \Exception('Invalid environment');
        }
    }

    /**
     * Check if the environment is dev
     *
     * @return bool
     */
    public static function isDevelopment(){
        return self::getEnvironment() == self::DEVELOPMENT;
    }

    /**
     * Check if the environment is live
     *
     * @return bool
     */
    public static function isLive(){
        return self::getEnvironment() == self::LIVE;
    }

    /**
     * Check if the environment is staging
     *
     * @return bool
     */
    public static function isStaging(){
        return self::getEnvironment() == self::STAGING;
    }

    /**
     * Set the passed config file
     *
     * @param string $config
     *
     * @throws \Exception
     */
    private static function setConfig($config)
    {
        //If it's not an array or object, or if it's NULL, then it's not a valid config.
        if( (!is_array($config) && !is_object($config)) || $config == NULL ){
            throw new \Exception('Invalid config file parsed');
        }

        self::$config = $config;
    }

    /**
     * Load the contents from a file using a parser.
     * @param string $filename
     *
     * @return string
     *
     * @throws \Exception
     */
    private static function getContents($filename){
        $parser = \Pixasia\Parser\ParserFactory::getInstance($filename);
        return $parser::parse($filename);
    }

    public static function loadConfig($contents)
    {
        try {
            self::setConfig($contents);
            self::setEnvironment();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        //Try and read everything from file into self::$config.
        $server = self::$environment;
        self::$config = (object) self::$config->$server;

        return self::$config;
    }

    /**
     * Config constructor.
     * @param string $filename
     *
     * @throws \Exception Throws an exception if fails to parse config
     */
    public function __construct($filename)
    {
        try {
            $contents = self::getContents($filename);
            self::loadConfig($contents);
        } catch (\Exception $e) {
            throw new \Exception('Failed to load ' . $filename . ':' . $e->getMessage());
        }
    }

    /**
     * Get the currently set environment
     *
     * @return string
     */
    public static function getEnvironment()
    {
        return static::$environment;
    }

    /**
     * Get a property from the loaded config. Accepts dotted notation to work through arrays.
     *
     * @param string $property Property to return from the loaded config
     * @param mixed  $fallback The value to return if the property isn't found.
     *
     * @throws \Exception Throws an exception if the property isn't found
     *
     * @return mixed
     */
    public static function get($property, $fallback = null)
    {
        $items = explode('.', $property);
        $search = static::$config;
        foreach ($items as $item) {
            if (isset($search->$item)) {
                $search = $search->$item;
            } else {
                if( $fallback === null){
                    throw new \Exception('Config not found for '.$property);
                }else{
                    return $fallback;
                }
            }
        }

        return $search;
    }
}
