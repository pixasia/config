<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 29/10/2016
 * Time: 20:42
 */

namespace Pixasia\Parser;

/**
 * Class ParserFactory
 *
 * Factory class to return a parser for a given filetype
 * @package Pixasia\Parser
 */
class ParserFactory{

    /**
     * Gets the extension of a given file.
     *
     * @param string $filename The filename to get the extension of
     *
     * @return string
     */
    private static function getExtension($filename){
       return ucfirst(pathinfo($filename)['extension']);
    }

    /**
     * Get the class name for a given type of parser
     *
     * @param string $type The required parser class
     *
     * @return string
     */
    private static function getParserClassName($type){
        return __NAMESPACE__.'\\'.$type;
    }

    /**
     * Factory class to return a parser for a given file type
     *
     * @param string $filename The file to return a parser for.
     * @return IParser
     *
     * @throws \Exception
     */
    public static function getInstance($filename){
        $type = self::getExtension($filename);

        //If the class doesn't exist - throw an exception
        if( !class_exists( self::getParserClassName($type) ) ){
            throw new \Exception('Unsupported file type: '.$type);
        }

        //Get the parser class as a variable to instantiate.
        $parser = self::getParserClassName($type);

        return new $parser;
    }

}