<?php
/**
 * Created by PhpStorm.
 * User: Sammy
 * Date: 19/10/2016
 * Time: 21:08
 */

namespace Pixasia\Parser;

/**
 * Class Json
 * @package Pixasia\Parser
 *
 * JSON parser to handle json configs
 */
class Json implements IParser
{
    /**
     * Load file from disk
     *
     * @param string    $filename   Load the given file from disk using file_get_contents
     *
     * @return string Returns a string of contents from the loaded file
     *
     * @throws \Exception Will throw an exception if the file cannot be read
     */
    private static function load($filename)
    {
        //Check filename exists
        if ( empty($filename) || $filename == NULL || !is_readable($filename) ) {
            throw new \Exception('Invalid config file location');
        }

        $contents = file_get_contents($filename);

        return $contents;
    }

    /**
     * Parse the given contents and JSON decode them
     *
     * @param string $contents  The contents read from disk
     *
     * @return \stdClass An object of decoded config variables
     *
     * @throws \Exception   An exception is thrown if there is invalid JSON
     */
    private static function parseContent($contents){
        $contents = json_decode($contents);

        if ($contents === NULL) {
            throw new \Exception('Invalid json file');
        }

        return $contents;
    }

    /**
     * Load a JSON encoded config file and return an array
     *
     * @param string $filename  The file to load the config from
     *
     * @return \stdClass An object containing the decoded config
     *
     * @throws \Exception
     */
    public static function parse($filename)
    {
        $contents = NULL;

        try {
            $contents = self::load($filename);
            $contents = self::parseContent($contents);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return (object)$contents;
    }
}