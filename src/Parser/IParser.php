<?php

namespace Pixasia\Parser;

/**
 * Interface IParser
 * @package Pixasia\Parser
 *
 * Interface to handle how each parser should handle files
 */
interface IParser{

    /**
     * Parse
     *
     * Load a file from disk and parse it in the required format
     *
     * @param string $filename The file to try and load the config from
     *
     * @return \stdClass Returns an object of parsed data
     */
    public static function parse($filename);

}